//INCLUDE EXPRESS LIBRARY
const express = require('express');     //importa expresss
const app = express();                  //iniziliazza express e ne assegna il valore all'interno della var app

//INCLUDE MONGODB CLIENT LIBRARY
const MongoDB = require('mongodb').MongoClient;     //richiamo MongoDB

//DECLARE THE PARAMETERS TO CONNECT TO MongoDB SERVER AND DB
const MongoDBUrl = 'mongodb://localhost:27017';
const MongoDBDataBaseName = 'rest-api';     //nome del compass


const writeUserToDB = (req, res) => {
    
    MongoDB.connect(MongoDBUrl, (err, client) => {
        const db = client.db(MongoDBDataBaseName);
        const collection = db.collection('users');

        var myUsername = req.query.name;
        var myUser = { name : myUsername, role : 'admin' };

        collection.insert(myUser, (err, result) => {
            res.send('Object created: ' + JSON.stringify(myUser) );
        });

        client.close();
    });
}

const readUserFromDB = (req, res) => {
    
    MongoDB.connect(MongoDBUrl, (err, client) => {
        const db = client.db(MongoDBDataBaseName);
        const collection = db.collection('users');

        collection.find({}).toArray((err, docs) => {
            res.send(JSON.stringify(docs) );
        });

        client.close();
    });
}

const readOneUserFromDB = (req, res) => {
    
    MongoDB.connect(MongoDBUrl, (err, client) => {
        const db = client.db(MongoDBDataBaseName);
        const collection = db.collection('users');

        var nameToSearch = req.query.name;

        collection.find({ name : nameToSearch }).toArray((err, docs) => {
            res.send(JSON.stringify(docs) );
        });

        client.close();
    });
}

app.get('/writeUsers', writeUserToDB);
app.get('/readUsers', readUserFromDB);
app.get('/readOneUser', readOneUserFromDB);

app.get('/', (req, res) => {            //arrow function
    var input = req.query.name;        
    res.send('L\'utente si chiama: ' +input);
} );

app.listen(3000, () => {
    console.log('Server ready!');
});



